from django.apps import AppConfig


class SiteDbConfig(AppConfig):
    name = 'site_db'
