from django.db import models
from django.urls import reverse
from django.contrib.postgres.fields import ArrayField

# Create your models here.
#Model that represents the movie items displayed in the website.
class Movies(models.Model):
    title = models.CharField(max_length=200)
    overview = models.TextField()
    original_language = models.TextField()
    genres = ArrayField(models.CharField(max_length=200), blank=True)
    homepage = models.URLField(null=True, blank=True)
    release_date = models.DateField('Release Date')
    runtime = models.IntegerField()
    total_likes = models.IntegerField(default=0)

    def get_absolute_url(self):
        return reverse('movie_details', kwargs={'pk':self.id})
#Model that represents the users of the movie site
class Users(models.Model):
    username = models.CharField(blank=False, null=False, max_length=70, unique=True)
    email = models.EmailField(unique=True)

# Model that represents a user's comments on a particular movie
class Comments(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movies, on_delete=models.CASCADE)
    comment = models.TextField(blank=False, help_text='Enter a comment')

#Model that represents likes for a movie
class Likes(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    movie = models.ForeignKey(Movies, on_delete=models.CASCADE)
