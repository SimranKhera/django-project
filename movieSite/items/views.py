from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, View
from .models import Movies
from .models import Likes
from .models import Users
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
from django.shortcuts import render, get_object_or_404,redirect
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from Member.models import Profile
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist
from urllib.parse import urlencode
# Index/home view for the Home page of the website.
def index(request):
    context = {}
    context['title'] = 'Home'
    return render(request, 'items/home.html', context)

#Displays the movies that a user liked.
class LikedMoviesView(ListView):
    template_name = 'items/likedMovies_list.html'
    paginate_by = 8
    model = Likes
    extra_context = {'title':'Liked Movies List'}
    ordering = ('-pk')
    #Retrieves all the movies the user liked.
    def get_queryset(self):
        profile = Profile.objects.filter(user = self.request.user).get()
        user_obj= Users.objects.filter(username=profile.user.username).get()
        object_list = Likes.objects.filter(user=user_obj)

        return object_list
# Like view to allow the registered users to like movies.
# Redirects the user to the movies list he liked.
class LikeView(LoginRequiredMixin,View):
    def get_success_url(self):
        return reverse("liked_movies")
    #Saves the user's like to the database for the specific movie they liked.
    def post(self, request, *args, **kwargs):
        like = Likes()
        like.movie = get_object_or_404(Movies, id=request.POST.get('movie_id'))
        profile = Profile.objects.filter(user =request.user).get()
        like.user =  Users.objects.filter(username = profile.user.username).get()
        like.save()
        return redirect(self.get_success_url())
#Displays a list of all the movies
class HomePageView(TemplateView):
    template_name = 'items/all_movies.html'
    extra_context = {'title': 'Movies'}
# The view that holds all the stored movies.
# If a query has been made, filter and order the movies based on the given values.
class GetAllMoviesList(ListView):
    template_name = 'items/all_movies.html'
    paginate_by = 5
    model = Movies
    ordering = ('-pk')
    def get_queryset(self):
        query = self.request.GET.get('search')
        filter_by = self.request.GET.get('filter_by')
        sort_by = self.request.GET.get('sort_by')
        page = self.request.GET.get('page')
        if query:
            print('HAS QUERY')
            if filter_by=='title':
                if 'ASC' in sort_by:
                    object_list = Movies.objects.filter(title__icontains=query).order_by(sort_by.replace('ASC',''))
                elif 'DESC' in sort_by:
                    object_list = Movies.objects.filter(title__icontains=query).order_by('-'+sort_by.replace('DESC',''))
            elif filter_by=='genres':
                if 'ASC' in sort_by:
                    object_list = Movies.objects.filter(genres__icontains=query).order_by(sort_by.replace('ASC',''))
                elif 'DESC' in sort_by:
                    object_list = Movies.objects.filter(genres__icontains=query).order_by('-'+sort_by.replace('DESC',''))
            return object_list
        else:
            return Movies.objects.all()
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        gt = self.request.GET.copy()
        if 'page' in gt:
            del gt['page']
        context['params'] = urlencode(gt)
        context['title'] = 'Movies List'
        return context


# The view that holds the details of a specific movie.
# Contains the Like button that allow the users to like the movie.
# The user needs to be registered in order to view the details.
class GetMovieDetail(LoginRequiredMixin,PermissionRequiredMixin,DetailView):
    template_name = 'items/movie_details.html'
    extra_context = {'title':'Movie Details'}
    model = Movies
    permission_required = 'items.view_movies'
# The view that lets a registered user with permissions add a new movie.
# The homepage field is optional so it can be left blank.
# After the movie has been successfully created, it redirects the user to the list of movies.
# A user must have the permission to create a movie.
class MovieCreateView(LoginRequiredMixin,PermissionRequiredMixin, CreateView):
    model = Movies
    fields = ['title','overview', 'original_language', 'genres', 'homepage', 'release_date', 'runtime', 'total_likes']
    permission_required = 'items.add_movies'
    success_url = reverse_lazy('movies_list')
    extra_context = {'title':'Create a movie'}

# The view that lets a registered user with permissions edit a movie.
# After the movie has been successfully created, it redirects the user to the list of movies.
# A user must have the permission to edit a movie.
class MovieEditView(LoginRequiredMixin,PermissionRequiredMixin, UpdateView):
    model = Movies
    fields = ['title','overview', 'original_language', 'genres', 'homepage', 'release_date', 'runtime', 'total_likes']
    extra_context = {'edit':'Edit the movie'}
    permission_required = 'items.change_movies'
    success_url = reverse_lazy('index')
#Allows the user to delete a movie
# The user must have the permission to delete a movie.
# The view that lets the registered user confirm a movie deletion.
# If the user confirms the deletion, he is redirected to the list of movies.
class MovieDeleteView(LoginRequiredMixin, PermissionRequiredMixin,DeleteView):
    model = Movies
    success_url = reverse_lazy('movies_list')
    extra_context = {'title':'Delete a movie'}
    permission_required =  'items.delete_movies'
