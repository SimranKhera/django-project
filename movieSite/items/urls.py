from django.urls import path
from . import views

urlpatterns = [path('', views.index, name='index'),
               path('index/', views.index, name='index'),
               path('movieslist/', views.GetAllMoviesList.as_view(),name='movies_list'),
               path('movie/<int:pk>/', views.GetMovieDetail.as_view(),name='movie_details'),
               path('movie/create/', views.MovieCreateView.as_view(),name='movie_create'),
               path('movie/edit/<int:pk>/', views.MovieEditView.as_view(),name='movie_edit'),
               path('movie/delete/<int:pk>/', views.MovieDeleteView.as_view(),name='movie_delete'),
               path('movie/like/<int:pk>/', views.LikeView.as_view(), name='like_movie'),
               path('likedmovies/', views.LikedMoviesView.as_view(), name='liked_movies'),
               ]
