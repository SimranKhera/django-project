# Generated by Django 3.1.3 on 2020-12-24 17:26

import django.contrib.postgres.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('overview', models.TextField()),
                ('original_language', models.TextField()),
                ('genres', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=200), blank=True, size=None)),
                ('homepage', models.URLField(blank=True, null=True)),
                ('release_date', models.DateField(verbose_name='Release Date')),
                ('runtime', models.IntegerField()),
                ('total_likes', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=70, unique=True)),
                ('email', models.EmailField(max_length=254, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Likes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('like', models.BinaryField(default=0)),
                ('movie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.movies')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.users')),
            ],
        ),
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.TextField(help_text='Enter a comment')),
                ('movie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.movies')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.users')),
            ],
        ),
    ]
