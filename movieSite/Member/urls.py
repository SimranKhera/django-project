from django.urls import path
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    path('register/', views.signup, name='member_register'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='member_login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='member_logout'),
    path('update_profile/',views.update_profile,name ="update_profile"),
]
