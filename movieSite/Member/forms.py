from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from items.models import Users
from .models import Profile
#Form for user registration
#This form has a username,email, password and password confirmation fields.
class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'password1','password2' )
class ImageUploadForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['name','picture']
# Form for user data
# This form has a username field and an email field.
class UserUpdateForm(forms.ModelForm):
    class Meta:
        model=User
        fields=['username','email']
# Form for user data
# This form has a username field and an email field.
class SiteUserUpdateForm(forms.ModelForm):
    class Meta:
        model=Users
        fields=['username','email']
#Form for profile data
#This form has a name field and picture field.
class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model=Profile
        fields=['name','picture']
