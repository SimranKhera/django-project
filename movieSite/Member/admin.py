from django.contrib import admin
from .models import Profile
from items.models import Movies,Users
# Register your models here.
from django.contrib import admin
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.contrib.admin.models import LogEntry

#Registers the Profile model to the admin site
admin.site.register(Profile)
admin.site.register(Movies)
admin.site.register(Users)

#Sets the superuser
# The superuser's username is 'nasr' and the password is 123
def create_super_user():
    created =   User.objects.get_or_create(username ='nasr')[0]
    if not created:
        superuser = User.objects.create(username='nasr')
        superuser.set_password('123')
        superuser.is_staff = True
        superuser.is_superuser = True
        superuser.save()
        Users.objects.get_or_create(username='nasr',email ='nasr@gmail.com')
        profile_one,created = Profile.objects.get_or_create(name = 'nasr',user = superuser)
        profile_one.save()

#Creates user groups
#Assigns CRUD operations permissions for items to  the admin_Item_gp group and for users
#to the admin_user_go group
def create_user_groups():
    content_type_movies = ContentType.objects.get(app_label='items', model='movies')
    content_type_user = ContentType.objects.get(app_label='items', model='users')
    admin_Item_gp,created = Group.objects.get_or_create(name='admin_Item_gp')
    admin_user_go,created = Group.objects.get_or_create(name='admin_user_go')
    members,created =  Group.objects.get_or_create(name='members')
    permissionItems = [ Permission.objects.get(codename='change_movies', content_type=content_type_movies),
    Permission.objects.get(codename='view_movies', content_type=content_type_movies) ,
    Permission.objects.get(codename='delete_movies',content_type=content_type_movies),
    Permission.objects.get(codename='add_movies',content_type=content_type_movies),]
    permissionUsers= [ Permission.objects.get(codename='change_users',content_type=content_type_user),
    Permission.objects.get(codename='view_movies',content_type=content_type_movies),
    Permission.objects.get(codename='change_users',content_type=content_type_user),
    Permission.objects.get(codename='view_users',content_type=content_type_user),
    Permission.objects.get(codename='add_users',content_type=content_type_user),
    Permission.objects.get(codename='delete_users',content_type=content_type_user),]
    permissionMembers,created = Permission.objects.get_or_create(codename='view_movies', content_type=content_type_movies)
    admin_Item_gp.permissions.set(permissionItems)
    admin_user_go.permissions.set(permissionUsers)
    members.permissions.add(permissionMembers)
#Creates users and assigns them to the admin_Item_gp group
def add_item_users():
    simran,created =  User.objects.get_or_create(username='simran')
    dania,created =  User.objects.get_or_create(username='dania')
    Users.objects.get_or_create(username='simran',email ='simran@gmail.com')
    Users.objects.get_or_create(username='dania',email ='dania@gmail.com')
    simran.set_password('123')
    dania.set_password('123')
    simran.is_staff=True
    dania.is_staff=True
    group = Group.objects.get(name='admin_Item_gp')
    simran.groups.add(group)
    dania.groups.add(group)
    simran.save()
    dania.save()
    profile_one,created = Profile.objects.get_or_create(name = 'simran',user = simran)
    profile_one.save()
    profile_two,created = Profile.objects.get_or_create(name = 'dania',user=dania)
    profile_two.save()
#Creates users and assigns them to the admin_user_go group
def add_group_users():
    xinjia,created = User.objects.get_or_create(username='xinjia')
    Users.objects.get_or_create(username='xinjia',email ='xinjia@gmail.com')
    Users.objects.get_or_create(username='keren',email ='keren@gmail.com')
    keren,created = User.objects.get_or_create(username ='keren')
    xinjia.set_password('123')
    keren.set_password('123')
    group = Group.objects.get(name='admin_user_go')
    xinjia.groups.add(group)
    keren.groups.add(group)
    xinjia.is_staff=True
    keren.is_staff=True
    xinjia.save()
    keren.save()
    profile_one,created = Profile.objects.get_or_create(name = 'xinjia',user = xinjia)
    profile_one.save()
    profile_two,created = Profile.objects.get_or_create(name = 'keren',user = keren)
    profile_two.save()

#Creates the log entries for every admin action
class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'user', 'action_time')

create_super_user()
create_user_groups()
add_item_users()
add_group_users()

#Registers the LogEntry and LogEntry models to the admin site
admin.site.register(LogEntry, LogEntryAdmin)
