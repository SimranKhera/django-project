from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from Member.forms import SignUpForm
from django.contrib.auth.models import Group
from .forms import UserUpdateForm,ProfileUpdateForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import login, authenticate
from .forms import ImageUploadForm
from .models import Profile
from items.models import Users
#Registers a user
#Adds the user to the members group
#Creates a profile for the user, and allows them to choose/set their username, profile picture and email
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        image_upload_form = ImageUploadForm(request.POST,request.FILES)
        if form.is_valid():
            if not image_upload_form.is_valid():
                raise ValueError('Invalid image upload')
            else:
                user = form.save()
                #Add user to the members group
                members =  Group.objects.get(name='members')
                members.user_set.add(user)
                user.refresh_from_db()
                user.save()
                raw_password = form.cleaned_data.get('password1')
                name = form.cleaned_data.get('username')
                user = authenticate(username=user.username, password=raw_password)
                siteUser = Users(username =name,email =form.cleaned_data.get('email'))
                siteUser.save()
                picture = form.cleaned_data.get('picture',None)
                if picture:
                    profile = Profile.objects.create(name = name,user = user,picture = picture)
                else:
                    profile = Profile.objects.create(name = name,user = user)
                profile.save()
                login(request, user)
                return redirect('/items/')
    else:
        form = SignUpForm()
        image_upload_form = ImageUploadForm(request.POST,request.FILES)
    context= {"form": form,
    "imageform":image_upload_form,'title' :'Sign Up'}
    return render(request, 'signup.html', context)


#Updates a user's profile
#The user must be logged in to update his profiled
#The user can update his name,username, email and profile picture.
#The updated data must be valid (username and email must both be unique)
@login_required
def update_profile(request):
    theuser = Users.objects.filter(username=request.user.username).get()
    if request.method == 'POST':
        p_form = ProfileUpdateForm(request.POST,request.FILES,instance=request.user.profile)
        u_form = UserUpdateForm(request.POST,instance=request.user)
        if p_form.is_valid() and u_form.is_valid():
            u_form.save()
            p_form.save()
            theuser.username = request.user.username
            theuser.email = request.user.email
            theuser.save()
            messages.success(request,'Your Profile has been updated!')
            return redirect('movies_list')
    else:
        initial_p = {'picture': request.user.profile.picture,'name': request.user.profile.name}
        initial_u = {'username': request.user.username,'email': request.user.email}
        p_form = ProfileUpdateForm(instance=request.user,initial = initial_p)
        u_form = UserUpdateForm(instance=request.user.profile,initial = initial_u)

    context={'p_form': p_form, 'u_form': u_form, 'title':'Update Profile'}
    return render(request, 'update_profile.html',context )
