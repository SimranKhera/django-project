from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
#Model for a user's profile
#The profile is associated with one user.
#The profile has a name field and a picture field.
#Those fields can be set through the user registration form
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length = 120,null=True)
    picture = models.ImageField(upload_to="pictures_upload/",default = "default_user_pic.png")
    def __str__(self):
        return str(self.user.first_name) + " Profile"
