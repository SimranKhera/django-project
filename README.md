# Django Project

Website link: 


This python project uses django to create exciting application for rating movies acquired from a database.
It displays a list of movies from postgres database. 

A guest will be welcomed with the home page. They may try to edit/delete/add movies but they will be redirected to the login page. 
Only admin/staff can add/delete/edit movies. If a logged in user tries to do these tasks without being a staff member, they will be a 
shown custom forbidden page. 

A Guest can only view the movies(without details). 

When a user registers (by clicking on login and register links), they may like a movie and view movie details. 
They will have access to a list of liked movies where they can find all the movies they have liked on the web app. 

A logged in user can also edit their profile by clicking update profile on the top right. They can change their avatar and other field.
A users avatar will always appear on the top right corner. 

Movie list can be be search and filtered. 

There is an admin user created: 
	username: nasr
	password: 123
And there are staff users created where only xinjia and keren can manage users and only simran and dania can manage movies:
	username: xinjia
	password: 123
	username: keren
	password: 123
	username: dania
	password: 123
	username: simran
	password: 123

These settings can be found in Member/admin.py

Heroku Link : https://dw-django-finalproject.herokuapp.com/
